#!/usr/bin/jython
from subprocess import PIPE, Popen
import sys
import pickle
sys.path.append("/app/stanford-iso-secops-proxy/netdb_support/netdb_rmi.jar")
from stanford.netdb import *

KRB5_KEYTAB = '/app/stanford-iso-secops-proxy/keytabs/securityops-netdb.keytab'
KRB5_PRINC = 'service/securityops-netdb@stanford.edu'
RMI_SERVICE = 'netdb.stanford.edu'

def lookup_node(host):
    """Takes a fqdn and does a NetDB RMI lookup for a node matching that name"""
    try:
        node = Node.load(host)
        fqdn = host
        try:
            os = [x.name() for x in node.oses()][0]
        except:
            os = None
        admins = []
        for admin_record in node.admins():
            try:
                admins.append(admin_record.netid())
            except:
                try:
                    # we may be looking at an admin team, let's enumerate the members
                    admin_team_name = admin_record.name()
                    admin_team = Admin_Team.load(admin_team_name)
                    for member in admin_team.members():
                        admins.append(member.netid())
                except:
                    pass
        try:
            admin_emails = ','.join(a.email() for a in node.admins())
        except:
            admin_emails = None
        try:
            user_emails = ','.join(a.email() for a in node.users())
        except:
            user_emails = None
        try:
            department = node.department().name()
        except:
            department = None
        temp = {'fqdn': fqdn,
                'admins': admins,
                'admin_emails': admin_emails,
                'os': os,
                'netdb_department': department,
                'user_emails': user_emails}
        return temp
    except:
        pass

if __name__ == '__main__':
    hosts = sys.argv[1:]
    # Initialize Kerberos session
    proc = Popen(['kinit', '-k', '-t', KRB5_KEYTAB, KRB5_PRINC], stdout=PIPE)
    output = Popen(['klist',], stdout=PIPE)
    # Create a NetDB datastore object and make it the default datastore
    ds = NetDB_Datastore(RMI_SERVICE)
    NetDB.default_datastore(ds)
    # Create result list and iterate through each host creating a new thread
    # for each one
    result = []
    for host in hosts:
        node = lookup_node(host)
        if node:
            result.append(node)
    print pickle.dumps(result)
