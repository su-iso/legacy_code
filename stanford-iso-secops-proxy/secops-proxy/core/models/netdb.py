import subprocess
import dns
import os
import time
import pickle
from dns import resolver, reversename

# jython wrapper because there is no native library :(
NETDB_WRAPPER = '/app/stanford-iso-secops-proxy/netdb_support/netdb_jython.py'
KRB5_KEYTAB = '/app/stanford-iso-secops-proxy/keytabs/securityops-netdb.keytab'
KRB5_PRINC = 'service/securityops-netdb@stanford.edu'

#initialize kerberos session
subprocess.check_output(['kinit', '-k', '-t', KRB5_KEYTAB, KRB5_PRINC], stderr=subprocess.STDOUT)

#defines function that wraps netdb cli binary to get node info
def get_node_info(input):
    details = {}
    if isinstance(input, list):
        full_node_list = input
    else:
        full_node_list = [input,]
    
    while len(full_node_list) > 0:
        node_list = full_node_list[-100:]
        full_node_list = full_node_list[:-100]
        os.environ['HOME'] = '/var/www'
        raw = subprocess.check_output(NETDB_WRAPPER.split() + node_list)
        nodes = pickle.loads(raw)
        for node in nodes:
            fqdn = node.get('fqdn').lower()
            if not details.get(fqdn):
                details[fqdn] = node
    return details

def dns_get_ptrs(ips):
    print ips
    ips_to_names = {}
    while len(ips) > 0:
        ips_slice = ips[-100:]
        ips = ips[:-100]
        for ip in ips_slice:
            fqdn = dns_get_ptr(ip)
            if fqdn and fqdn != 'none':
                ips_to_names[ip] = fqdn
    return ips_to_names

def dns_get_ptr(ip, resolvers=['171.64.1.234', '171.67.1.234']):
    addr = dns.reversename.from_address(ip)
    r = dns.resolver.Resolver()
    r.nameservers = resolvers
    r.timeout = 1
    r.lifetime = 1
    attempts = 1
    a = None
    while True:
        try:
            a = r.query(addr, 'PTR')
            break
        except dns.exception.Timeout:
            time.sleep(2^attempts)
            attempts += 1
        except dns.resolver.NXDOMAIN:
            break
    if a and a.response and a.response.answer:
        answer = ",".join([str(x[-1])[:-1] for x in a.response.answer])
    else:
        answer = None
    return answer
