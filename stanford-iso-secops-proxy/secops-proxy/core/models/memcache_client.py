## Stanford SecOps Proxy
## Author: Thomas Zakrajsek <tzakrajs@stanford.edu>
## File: core/models/memcache_client.py

from core import application
import hashlib
import logging
import json
import pylibmc

# Define globals
MEMCACHE_HOST = application.config.get('memcache.host', '127.0.0.1')

# Create shared memcache connection object
mc = pylibmc.Client([MEMCACHE_HOST])

# Create logging object
log = logging.getLogger('core')

def hash_key(key):
    return hashlib.sha256(str(key)).hexdigest()

def get_wrapped(key, is_json=False):
    key = hash_key(key)
    try:
        value = mc.get(key)
    except:
        value = None
    finally:
        mc.disconnect_all()
    if value:
        if is_json:
            value = json.loads(value)
        return value
    else:
        return None

def set_wrapped(key, value, is_json=False, expires=0):
    key = hash_key(key)
    if is_json:
        value = json.dumps(value)
    try:
        mc.add(key, value, expires)
    except Exception as e:
        log.error("Failed to add key: {0} to memcache - {1}".format(key, e))
    finally:
        mc.disconnect_all()

def delete_wrapped(key):
    key = hash_key(key)
    try:
        mc.delete(key)
    except Exception as e:
        log.error("Failed to remove key: {0} from memcache - {1}".format(key, e))
    finally:
        mc.disconnect_all()
