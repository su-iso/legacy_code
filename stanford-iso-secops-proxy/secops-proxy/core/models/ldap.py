import subprocess, re

LDAPSEARCH_PATH = '/usr/bin/ldapsearch'
LDAP_HOST = 'ldap.stanford.edu'
KRB5_KEYTAB = '/app/stanford-iso-secops-proxy/keytabs/iso-qualys-dashboard.keytab'
KRB5_PRINC = 'service/iso-qualys-dashboard@stanford.edu'

#initialize kerberos session
print subprocess.check_output(['kinit', '-k', '-t', KRB5_KEYTAB, KRB5_PRINC], stderr=subprocess.STDOUT)

def get_email_using_unique(unique_id): 
    try:
        raw = subprocess.check_output([LDAPSEARCH_PATH, '-h', LDAP_HOST, '-b', 'cn=people,dc=stanford,dc=edu', 'suUniqueIdentifier={}'.format(unique_id), 'mail'])
    except:
        raw = ''
    if 'ERROR' in raw:
        raise RuntimeError(raw.split(':')[3])
    try:
        email = [x.split(' ')[1] for x in raw.split('\n') if x.startswith('mail:')][0]
    except:
        return None
    return email
