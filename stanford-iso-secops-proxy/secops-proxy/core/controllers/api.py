## Stanford SecOps Proxy
## Author: Thomas Zakrajsek <tzakrajs@stanford.edu>
## File: core/controllers/api.py

"""Defines the routes for the SecOps Proxy API"""

from bottle import abort, error, request, response
import logging
import json
import smtplib
import time
from uuid import UUID

from core import application
from core.models.netdb import get_node_info, dns_get_ptr, dns_get_ptrs

# Create logging object
log = logging.getLogger('core')

@application.route('/api/current/netdb/node/<name>', method='GET')
@application.route('/api/1.0/netdb/node/<name>', method='GET')
def get_netdb_node(name):
    """Returns a JSON object with the NetDB node metadata"""
    node = get_node_info(name)
    return json.dumps(node[name])

@application.route('/api/current/netdb/nodes', method='POST')
@application.route('/api/1.0/netdb/nodes', method='POST')
def get_netdb_nodes():
    """Returns a JSON array with all of the NetDB nodes indexed by name"""
    # Ensure that we can parse the JSON post data
    try:
        body_raw = request.body.read()
        body = json.loads(body_raw)
    except Exception as e:
        log.warning("Invalid JSON POSTed: {0} - {1}".format(body_raw, e))
        abort(400, "Invalid JSON detected: {0}".format(e))
    # Make sure they are giving us a list
    if isinstance(body, list):
        # Get the node info and return the dict as a JSON blob
        nodes = get_node_info(body)
        return json.dumps(nodes)
    else:
        abort(400, "JSON Object is not a list!")

@application.route('/api/current/dns/ptr/<ip>', method='GET')
@application.route('/api/1.0/dns/ptr/<ip>', method='GET')
def get_dns_ptr(ip):
    """Returns a string with the PTR record for the IP"""
    ptr = dns_get_ptr(ip)
    return ptr

@application.route('/api/current/dns/ptrs', method='POST')
@application.route('/api/1.0/dns/ptrs', method='POST')
def get_dns_ptrs():
    """Returns a JSON array with all of the PTRs indexed by IP"""
    # Ensure that we can parse the JSON post data
    try:
        body_raw = request.body.read()
        body = json.loads(body_raw)
    except Exception as e:
        log.warning("Invalid JSON POSTed: {0} - {1}".format(body_raw, e))
        abort(400, "Invalid JSON detected: {0}".format(e))
    # Make sure they are giving us a list
    if isinstance(body, list):
        # Get the node info and return the dict as a JSON blob
        ptrs = dns_get_ptrs(body)
        return json.dumps(ptrs)
    else:
        abort(400, "JSON Object is not a list!")

@application.route('/api/current/email', method='POST')
@application.route('/api/1.0/email', method='POST')
def email():
    """Sends emails to the recipient addresses from the to address provided.
       It requires a from_address, to_address and message (subject and body).
    """
    # Ensure that we can parse the JSON post data
    try:
        body_raw = request.body.read()
        body = json.loads(body_raw)
    except Exception as e:
        log.warning("Invalid JSON POSTed: {0} - {1}".format(body_raw, e))
        abort(400, "Invalid JSON detected: {0}".format(e))
    # Make sure they are giving us a dictionary
    if isinstance(body, dict):
        from_address = body.get('from_address')
        to_address = body.get('to_address')
        subject = body.get('subject')
        msg = body.get('msg')
    else:
        abort(400, "JSON object is not a dictionary!")
    if not from_address and to_address and subject and msg:
        abort(400, "Missing from_address, to_address, subject, and/or msg")
    # now that we know we have a proper email request, lets send the bloody
    # thing!
    try:
        server = smtplib.SMTP_SSL('smtp.stanford.edu')
        server.sendmail(from_address, to_address, msg)
        server.quit()
    except:
        abort(500, "Something went wrong when sending email via SMTP")
